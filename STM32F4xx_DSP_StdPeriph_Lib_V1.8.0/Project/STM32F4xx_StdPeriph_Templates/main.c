#include "stm32f4xx_conf.h"

#define BAUDRATE 9600
#define ON '1'
#define OFF '0'

char _bufferUSART1, _bufferUSART6 = OFF;
void ToggleLed(void);

/* 
 * -----------------------------------------------------------------------------
 *                                   M A I N
 * -----------------------------------------------------------------------------
 */

int main(void)
{
	// Locals
	GPIO_InitTypeDef _gpioInit;
	EXTI_InitTypeDef _extiInit;
	USART_InitTypeDef _usartInit;
	NVIC_InitTypeDef _nvicInit;

	// Power
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	// GPIO General
	_gpioInit.GPIO_OType = GPIO_OType_PP;
	_gpioInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
	_gpioInit.GPIO_Speed = GPIO_Speed_100MHz;

	// GPIO - Transmission IOs
	_gpioInit.GPIO_Mode = GPIO_Mode_AF;
	_gpioInit.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12;
	GPIO_Init(GPIOA, &_gpioInit);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_USART6);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_USART6);

	// GPIO - LED
	_gpioInit.GPIO_Mode = GPIO_Mode_OUT;
	_gpioInit.GPIO_Pin = GPIO_Pin_5;
	GPIO_Init(GPIOA, &_gpioInit);

	// GPIO - Button
	_gpioInit.GPIO_Mode = GPIO_Mode_IN;
	_gpioInit.GPIO_OType = GPIO_OType_PP;
	_gpioInit.GPIO_Pin = GPIO_Pin_13;
	_gpioInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
	_gpioInit.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOC, &_gpioInit);

	// EXTI
		_extiInit.EXTI_Line = EXTI_Line13;
	_extiInit.EXTI_LineCmd = ENABLE;
	_extiInit.EXTI_Mode = EXTI_Mode_Interrupt;
	_extiInit.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_Init(&_extiInit);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

	// USART
	_usartInit.USART_BaudRate = BAUDRATE;
	_usartInit.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	_usartInit.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	_usartInit.USART_Parity = USART_Parity_No;
	_usartInit.USART_StopBits = USART_StopBits_1;
	_usartInit.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &_usartInit);
	USART_Init(USART6, &_usartInit);

	USART_Cmd(USART1, ENABLE);
	USART_Cmd(USART6, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART6, USART_IT_RXNE, ENABLE);

	// NVIC
	_nvicInit.NVIC_IRQChannel = EXTI15_10_IRQn;
	_nvicInit.NVIC_IRQChannelCmd = ENABLE;
	_nvicInit.NVIC_IRQChannelPreemptionPriority = 0;
	_nvicInit.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&_nvicInit);

	_nvicInit.NVIC_IRQChannel = USART1_IRQn;
	_nvicInit.NVIC_IRQChannelCmd = ENABLE;
	_nvicInit.NVIC_IRQChannelPreemptionPriority = 0;
	_nvicInit.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&_nvicInit);

	_nvicInit.NVIC_IRQChannel = USART6_IRQn;
	_nvicInit.NVIC_IRQChannelCmd = ENABLE;
	_nvicInit.NVIC_IRQChannelPreemptionPriority = 0;
	_nvicInit.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&_nvicInit);

	// Main
	while (1);
}

/* 
 * -----------------------------------------------------------------------------
 *                              H A N D L E R S
 * -----------------------------------------------------------------------------
 */

void EXTI15_10_IRQHandler()
{
	if (!GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13))
	{
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) != SET);
		USART_SendData(USART1, ON);
		while (USART_GetFlagStatus(USART6, USART_FLAG_TXE) != SET);
		USART_SendData(USART6, ON);
	}
	else
	{
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) != SET);
		USART_SendData(USART1, OFF);
		while (USART_GetFlagStatus(USART6, USART_FLAG_TXE) != SET);
		USART_SendData(USART6, OFF);
	}
	EXTI_ClearITPendingBit(EXTI_Line13);
}

void USART1_IRQHandler()
{
	_bufferUSART1 = USART_ReceiveData(USART1);
	ToggleLed();
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}

void USART6_IRQHandler()
{
	_bufferUSART6 = USART_ReceiveData(USART6);
	ToggleLed();
	USART_ClearITPendingBit(USART6, USART_IT_RXNE);
}

void ToggleLed()
{
	if (_bufferUSART1 || _bufferUSART6)
		GPIO_SetBits(GPIOA, GPIO_Pin_5);
	else
		GPIO_ResetBits(GPIOA, GPIO_Pin_5);
}
