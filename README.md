*I will continue using German language, because the task was given in german, 
and I dont' have the time to translate everything*  
  
# Microcontroller Programmierung - Abschlussprojekt
Sommersemester 2018

## Team Mitglieder
- [Sabrina Prochazka](https://gitlab.com/Lady_of_cinder)  
- [Richard Slezak](https://gitlab.com/richardvsj2008)  
- [Florian Traxler](https://gitlab.com/FTraxler)  

## Aufgabenstellung
**Aufgabe M (3 Personen): Programmieren Sie drei Nucleo-Boards (A, B und C) wie folgt:**  
Die Nucleo-Boards sind über USART-Schnittstellen miteinander verbunden. Zu Beginn sind alle UserLEDs  
dunkel. Wenn irgendein User-Button gedrückt wird, sollen die jeweils anderen User-LEDs  
leuchten (so lange der User-Button gedrückt ist). Wenn mehrere User-Buttons gleichzeitg gedrückt  
werden, sollen alle User-LEDs leuchten.  
Beispiele für die gewünschte Wirkungsweise:  
- kein User-Button gedrückt -> keine User-LED leuchtet  
- User-Button C gedrückt -> User-LEDs A und B leuchten  
- User-Buttons C und A gedrückt -> alle User-LEDs leuchten  
- User-Button A gedrückt -> User-LEDs B und C leuchten  
(usw…)  
  
## Sourcecode
[main.c](/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Project/STM32F4xx_StdPeriph_Templates/main.c)

## Wiki
[Wiki](https://gitlab.com/FTraxler-FH-Projects/2nd-Semester/MC-Project/wikis/home)